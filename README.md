# README #

An example iPhone UI for an email app

### What is this repository for? ###

* This is an example Unity project for an iPhone email app.
* Version 1.0

### How do I get set up? ###

* Download the entire project folder
* Open it in Unity
* Works as of Unity 5.1


![CellphoneAppScrnShot.png](https://bitbucket.org/repo/zkGdBp/images/454757028-CellphoneAppScrnShot.png)