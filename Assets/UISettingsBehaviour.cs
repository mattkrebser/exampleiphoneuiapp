﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;
using System.Text.RegularExpressions;
using UnityCellPhoneApp;

public class UISettingsBehaviour : MonoBehaviour {

	public bool activeOnStart = true;
	public GameObject prefab;
	public GameObject ContentPanel;
	public static GameObject activeObject;
	public GameObject warningThingy;

	void Start(){
		if (!activeOnStart)
			gameObject.SetActive (false);
	}


	public void SetDayText(GameObject ObjectToSet){
		ObjectToSet.GetComponent<Text> ().text = GetComponentInChildren<Text> ().text;
	}

	public void SetTextToThisSlider(GameObject textObject){
		textObject.GetComponent<Text> ().text = GetComponent<Slider> ().value.ToString ();
	}

	public void InvertActive(GameObject inputObject){
		if (inputObject.activeSelf)
			inputObject.SetActive (false);
		else
			inputObject.SetActive (true);
	}

	public void MakeEmailPrefab(GameObject textObject){

		if (!TestEmail.IsEmail (textObject.GetComponent<InputField> ().text)) {
			GameObject warn = Instantiate (warningThingy, Vector3.zero, Quaternion.identity) as GameObject;
			warn.GetComponent<DestroyThisAfterSeconds>().DestroyIfThisIsNotActive = gameObject.transform.parent.gameObject;
			warn.transform.SetParent(ContentPanel.transform.parent.parent.parent.parent);
			warn.GetComponent<RectTransform>().anchoredPosition3D = new Vector3(327,-693);
			textObject.GetComponent<InputField> ().text = "";
			return;
		}

		GameObject p = Instantiate (prefab, Vector3.zero, Quaternion.identity) as GameObject;
		p.GetComponent<RectTransform> ().localScale = new Vector3 (1, 1, 1);
		StartCoroutine (EnforeScale (p));
		p.GetComponentInChildren<Text> ().text = textObject.GetComponent<InputField> ().text;
		Communicator.AddEmail (textObject.GetComponent<InputField> ().text);
		p.transform.SetParent (ContentPanel.transform);
		textObject.GetComponent<InputField> ().text = "";
	}

	private IEnumerator EnforeScale(GameObject pooh){
		yield return 0;
		yield return 0;
		yield return 0;
		yield return 0;
		yield return 0;
		yield return 0;
		yield return 0;
		yield return 0;
		yield return 0;
		pooh.GetComponent<RectTransform> ().localScale = new Vector3 (1, 1, 1);
	}

	public void DestroyActiveObject(){
		if (activeObject != null) {
			Communicator.RemoveEmail(activeObject.GetComponentInChildren<Text>().text);
			Destroy (activeObject);
		}
	}

	public void SetActiveObject(){
		activeObject = gameObject;
	}

	public void SetDayRepeat(){
		Communicator.SetDayRepeat (GetComponentInChildren<Text> ().text);
	}

	public void SetHourRepeat(){
		Communicator.SetHourlyRepeat (GetComponentInChildren<Text> ().text);
	}

	public void SetHistoryAmount(){
		Communicator.SetHistoryAmount ((int)(GetComponent<Slider> ().value));
	}

	public void SetReportLength(){
		Communicator.SetDefaultMileReportDayLength ((int)(GetComponent<Slider> ().value));
	}

	public void SendReport(GameObject textObj){
		Communicator.SendReport (textObj.GetComponent<Text> ().text);
	}

	/// <summary>
	/// Tests an E-Mail address.
	/// </summary>
	public static class TestEmail
	{
		/// <summary>
		/// Regular expression, which is used to validate an E-Mail address.
		/// </summary>
		public const string MatchEmailPattern =
			@"^(([\w-]+\.)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@"
				+ @"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\."
				+ @"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
				+ @"([a-zA-Z]+[\w-]+\.)+[a-zA-Z]{2,4})$";
		
		
		/// <summary>
		/// Checks whether the given Email-Parameter is a valid E-Mail address.
		/// </summary>
		/// <param name="email">Parameter-string that contains an E-Mail address.</param>
		/// <returns>True, wenn Parameter-string is not null and contains a valid E-Mail address;
		/// otherwise false.</returns>
		public static bool IsEmail(string email)
		{
			if (email != null) return Regex.IsMatch(email, MatchEmailPattern);
			else return false;
		}
	}
}
