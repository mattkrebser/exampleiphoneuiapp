
using System;
using System.IO;
using UnityEngine;
using System.Collections.Generic;
using System.Xml.Serialization;
using System.Xml;

namespace UnityCellPhoneApp
{

    public static class FileManager
    {
        public static readonly string settingsName = "/Settings.dat";
        public static readonly string historyName = "/History.dat";


        public static string GetHistoryPath()
        {
            return Application.persistentDataPath + historyName;
        }

        public static string GetSettingsPath()
        {
            return Application.persistentDataPath + settingsName;
        }

        /// <summary>
        /// Writes to binary file.
        /// </summary>
        /// <param name="filePath">File path.</param>
        /// <param name="objectToWrite">Object to write.</param>
        /// <param name="append">If set to <c>true</c> append.</param>
        /// <typeparam name="T">The 1st type parameter.</typeparam>
        public static void WriteToXMLFile<T>(string filePath, T objectToWrite, bool append = false)
        {
            using (Stream stream = File.Open(filePath, append ? FileMode.Append : FileMode.OpenOrCreate))
            {
                var My_XMLSerializer = new XmlSerializer(typeof(T));
                My_XMLSerializer.Serialize(stream, objectToWrite);
            }
        }

        /// <summary>
        /// Writes to binary file.
        /// </summary>
        /// <param name="filePath">File path.</param>
        /// <param name="objectToWrite">Object to write.</param>
        /// <param name="append">If set to <c>true</c> append.</param>
        /// <typeparam name="T">The 1st type parameter.</typeparam>
        public static void OverWriteToXMLFile<T>(string filePath, T objectToWrite, bool append = false)
        {
            using (Stream stream = File.Create(filePath))
            {
                var My_XMLSerializer = new XmlSerializer(typeof(T));
                My_XMLSerializer.Serialize(stream, objectToWrite);
            }
        }

        /// <summary>
        /// Reads from binary file.
        /// </summary>
        /// <returns>The from binary file.</returns>
        /// <param name="filePath">File path.</param>
        /// <typeparam name="T">The 1st type parameter.</typeparam>
        private static T ReadFromXMLFile<T>(string filePath)
        {
            T newObject = default(T);
            using (Stream stream = File.Open(filePath, FileMode.Open))
            {

                var My_XMLSerializer = new XmlSerializer(typeof(T));
                var My_XMLReader = XmlReader.Create(stream);
                newObject = (T)My_XMLSerializer.Deserialize(My_XMLReader);
            }

            return newObject;
        }
    }

    [System.Serializable]
    public class Settings
    {
        public int HistoryLength = 7;
        public int DefaultReportLength = 7;
        public List<string> emails = new List<string>();
        public Day day = Day.none;
        public int hour = -1;
        public bool isAM = false;
    }

    public enum Day
    {
        Monday,
        Tuesday,
        Wednesday,
        Thursday,
        Friday,
        Saturday,
        Sunday,
        EveryDay,
        none
    }
}












