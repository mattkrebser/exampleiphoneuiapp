﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityCellPhoneApp;

public class HistoryFeature : MonoBehaviour {

	private static GameObject activeObject;


	public void setActiveObject(){
		activeObject = gameObject;
	}

	public void DeleteActiveObject(){
		if (activeObject != null) {
			Communicator.RemoveHistory(activeObject.GetComponentInChildren<Text>().text);
			Destroy (activeObject);
		}
	}
}
