﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityCellPhoneApp;

public class EnableDisableFeature : MonoBehaviour {

	public void SetSliderValueFromButton(GameObject slider){
		if (slider.GetComponent<Slider>().value == 0){
			slider.GetComponent<Slider>().value = 1;
		}
		else
			slider.GetComponent<Slider>().value = 0;
	}

	public void SetDisabledTextFromSlider(GameObject textObj){
		if (GetComponent<Slider> ().value == 0) {
			textObj.GetComponent<Text> ().text = "(Enabled)";
			Communicator.startService();
		}
		else{
			textObj.GetComponent<Text> ().text = "(Disabled)";
			Communicator.StopService();
		}
	}

	public void Exit(){
		Application.Quit ();
	}
}
