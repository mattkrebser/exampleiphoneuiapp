﻿using UnityEngine;
using System.Collections;

public class DestroyThisAfterSeconds : MonoBehaviour {

	public float SECONDS_UNTIL_DESTROY;
	// Use this for initialization
	public GameObject DestroyIfThisIsNotActive;
	void Start () {
		StartCoroutine (destroy_me ());
	}
	
	private IEnumerator destroy_me(){
		yield return 0;
		yield return 0;
		yield return 0;
		yield return 0;
		GetComponent<RectTransform> ().localScale = new Vector3 (1, 1, 1);
		float count = 0.0f;

		while (count < SECONDS_UNTIL_DESTROY){
			if (DestroyIfThisIsNotActive != null && !DestroyIfThisIsNotActive.activeSelf)
				break;
			yield return new WaitForSeconds(0.15f);
			count += 0.15f;
		}
		Destroy (gameObject);
	}
}
