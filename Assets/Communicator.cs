﻿using UnityEngine;
using System.Collections;
using System;
using UnityCellPhoneApp;

namespace UnityCellPhoneApp
{

    public static class Communicator
    {

        public static Settings currentSettings = new Settings();

        public static void RemoveHistory(string history)
        {
            Debug.Log("Removed " + history);
        }

        public static void SetDayRepeat(string day)
        {
            if (day == "Monday")
            {
                currentSettings.day = Day.Monday;
            }
            else if (day == "Tuesday")
            {
                currentSettings.day = Day.Tuesday;
            }
            else if (day == "Thursday")
            {
                currentSettings.day = Day.Thursday;
            }
            else if (day == "Friday")
            {
                currentSettings.day = Day.Friday;
            }
            else if (day == "Sunday")
            {
                currentSettings.day = Day.Sunday;
            }
            else if (day == "Saturday")
            {
                currentSettings.day = Day.Saturday;
            }
            else if (day == "Wednesday")
            {
                currentSettings.day = Day.Wednesday;
            }
            else if (day == "Every Day")
            {
                currentSettings.day = Day.EveryDay;
            }
            else if (day == "Never")
            {
                currentSettings.day = Day.none;
            }
        }

        public static void SetHourlyRepeat(string time)
        {
            if (time == "Never O' Clock")
            {
                currentSettings.hour = -1;
                return;
            }
            string number = time.Substring(0, time.IndexOf(':'));
            int num = -1;
            Int32.TryParse(number, out num);
            if (num == -1)
            {
                Debug.Log("no hour set");
                return;
            }
            else
            {
                currentSettings.hour = num;
                if (time.Contains("AM"))
                {
                    currentSettings.isAM = true;
                }
                else
                    currentSettings.isAM = false;
                return;
            }
        }

        public static void SetHistoryAmount(int amount)
        {
            currentSettings.HistoryLength = amount;
        }

        public static void SetDefaultMileReportDayLength(int length)
        {
            currentSettings.DefaultReportLength = length;
        }

        public static void SendReport(string days)
        {

        }

        public static void AddEmail(string address)
        {
            currentSettings.emails.Add(address);
        }

        public static void RemoveEmail(string address)
        {
            currentSettings.emails.Remove(address);
        }

        public static void startService()
        {

        }

        public static void StopService()
        {

        }
    }
}








