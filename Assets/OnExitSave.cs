﻿using UnityEngine;
using System.Collections;
using UnityCellPhoneApp;

public sealed class OnExitSave : MonoBehaviour {

	void OnApplicationPause(bool pauseStatus){
		FileManager.WriteToXMLFile<Settings> (FileManager.GetSettingsPath (), Communicator.currentSettings);
	}

	void OnApplicationQuit(){
		FileManager.WriteToXMLFile<Settings> (FileManager.GetSettingsPath (), Communicator.currentSettings);
	}
}
